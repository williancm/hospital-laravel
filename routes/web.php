<?php

/*
Home Routes
*/ 

Route::get('/', function () {
    return view('cliente.inicio');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get("inicio", 'HomeController@inicio')->name('home.inicio');

Route::get("show/{id}", 'HomeController@show')->name('home.show');

Route::get("createConsulta/{id}", 'HomeController@createConsulta')->name('home.createConsulta');

Route::post("saveConsulta/{id}", 'HomeController@saveConsulta')->name('home.saveConsulta');

Route::get("pesquisa", 'HomeController@pesquisa')->name('home.pesquisa');


/*
Doutors Routes
*/
Route::resource('doutores', 'DoutorController')->middleware('auth');

Route::get("createSetor", 'DoutorController@createSetor')->name('doutores.createSetor');

Route::post("saveSetor", 'DoutorController@saveSetor')->name('doutores.saveSetor');

Route::get("viewConsultas", 'DoutorController@viewConsultas')->name('doutores.viewConsultas');

Route::get("consultaDoutor", 'DoutorController@consultaDoutor')->name('doutores.consultaDoutor');

Route::get("relatorio", 'DoutorController@relatorio')->name('doutores.relatorio');

/*E-mail*/ 
Route::post('/send', 'EmailController@send')->name('email.send');


Auth::routes();