@extends('cliente.layout') 

@section('conteudo')

        <h2 value="{{$reg->nome or old('nome')}}"></h2>
        <div class="card img-fluid" style="width:500px">
            <img class="card-img-top" src='storage/fotos/{{ $reg->foto }}' alt="Card image" style="width:100%">
            <div class="card-img-overlay">
                CPF: <input type="text" class="form-control" id="cpf" name="cpf" value="{{$reg->cpf or old('cpf')}}" readonly="readonly">
                Sala: <input type="text" class="form-control" id="sala" name="sala" value="{{$reg->sala or old('sala')}}" readonly="readonly">
                Preço da Consulta: <input type="text" class="form-control" id="precoConsulta" name="precoConsulta" value="R$ {{$reg->precoConsulta or old('precoConsulta')}}" readonly="readonly">
                E-mail: <input type="text" class="form-control" id="email" name="email" value="{{$reg->email or old('email')}}" readonly="readonly">
                Setor: <input type="text" class="form-control" id="setor_id" name="setor_id" value="{{$reg->setor->setor}}" readonly="readonly">
                <br/>
                <a href="{{ route('home.createConsulta', $reg->id) }}" class="btn btn-primary">Marcar Consulta</a>
            </div>
        </div>

@endsection