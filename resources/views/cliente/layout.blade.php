<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hospital</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Hospital Consultas</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="{{ route('home.inicio') }}">Home</a></li>
    </ul>
    <form class="navbar-form navbar-left" action="{{ route('home.pesquisa') }}">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="palavra">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
      </div>
    </form>
  </div>
</nav>
  
<div class="container">

@yield('conteudo')  

</div>

</body>
</html>

