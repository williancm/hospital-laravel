@extends('cliente.layout') 

@section('conteudo')
@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif
<h3>Consulta</h3>
<form method="post" action="{{ route('home.saveConsulta', $reg->id) }}">
    {{ csrf_field() }}
    {{ method_field('post') }} 
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Nome:</label>
            <input type="text" class="form-control" id="nome" name="nome" 
                  value="{{old('nome')}}" autofocus> 
        </div>
      </div>
      <div class="col-sm-1">
        <div class="form-group">
          <label for="idade">Idade</label>
            <input type="text" class="form-control" id="idade" name="idade" 
                  value="{{old('idade')}}">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <div class="form-group">
          <label for="cpf">CPF:</label>
            <input type="text" class="form-control" id="cpf" name="cpf" 
                  value="{{old('cpf')}}" > 
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label for="email">E-mail:</label>
            <input type="text" class="form-control" id="email" name="email" 
                  value="{{old('email')}}">
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label for="data">Data:</label>
            <input type="date" id="data" name="data" 
                  value="{{old('data')}}">
        </div>
      </div>
    </div>
    
    <input type="submit" value="Marcar" class="btn btn-danger">
</form>
          

@endsection