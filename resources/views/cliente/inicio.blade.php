@extends('cliente.layout') 

@section('conteudo')
@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

    @foreach ($reg as $linha)
    <div class="card" style="width:400px">
    <img class="card-img-top" src='storage/{{ $linha->foto }}' alt="Card image" style="width:100%">
        <div class="card-body">
        <h4 class="card-title" style="font-color:white">{{ $linha->nome }}</h4>
        <p class="card-text">{{ $linha->setor->setor }}</p>
        <a href="{{ route('home.show', $linha->id) }}" class="btn btn-warning">Ver detalhes</a>
        </div>
    </div>
    <br/>
    @endforeach

@endsection