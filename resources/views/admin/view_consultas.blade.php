@extends('admin.modelo') 
@section('conteudo')

<div class="row">
  <div class="col-sm-10">
     <h3>Consultas Marcadas</h3>
  </div>   
</div>

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<table class="table table-hover">
  <thead>
    <tr>
    <th>Data</th>
      <th>Nome do Paciente</th>
      <th>E-mail</th>
      <th>Preço da Consulta</th>
      <th>Doutor</th>
      <th>Setor</th>
      <th>Lembrete</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($linhas as $linha)
    <tr>
      <td> {{ $linha->data }} </td> 
      <td> {{ $linha->nome }} </td> 
      <td> {{ $linha->email }} </td>
      <td> R$ {{ $linha->doutor->precoConsulta }} </td> 
      <td> {{ $linha->doutor->nome }} </td>
      <td> {{ $linha->doutor->setor->setor }} </td>
      <td> <a href="{{ route('email.send')}}" class="btn btn-danger btn-sm" role="button">Enviar Lembrete</a></td>
  </tr>

    @endforeach

  </tbody>
</table>
@endsection