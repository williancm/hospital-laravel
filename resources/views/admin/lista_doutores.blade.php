@extends('admin.modelo') 
@section('conteudo')

<div class="row">
  <div class="col-sm-10">
     <h3>Doutores Disponiveis</h3>
  </div>   
  <div class="col-sm-2">
    <a href="{{ route('doutores.create') }}" class="btn btn-primary btn-sm" style="margin-top:24px" role="button">Incluir</a>
  </div>   
</div>

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<table class="table table-hover">
  <thead>
    <tr>
    <th>Cód.</th>
      <th>Nome do Doutor</th>
      <th>Area</th>
      <th>Preço da Consulta</th>
      <th>E-mail</th>
      <th>Foto</th>
      <th>Ações</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($linhas as $linha)
    <tr>
      <td> {{ $linha->id }} </td> 
      <td> {{ $linha->nome }} </td> 
      <td> {{ $linha->setor->setor }} </td>
      <td> R$ {{ $linha->precoConsulta }} </td> 
      <td> {{ $linha->email }} </td>
      <td> <img src='storage/{{ $linha->foto }}' style='width: 120px; height: 80px;'> </td>
      <td> <a href="{{ route('doutores.edit', $linha->id) }}" class="btn btn-info btn-sm" role="button">Alterar</a>&nbsp; 
          <a href="{{ route('doutores.show', $linha->id) }}" class="btn btn-success btn-sm" role="button">Consultar</a>&nbsp; 
          <form method="post" action="{{ route('doutores.destroy', $linha->id)}}"
                style="display: inline-block"
                onsubmit="return confirm('Confirma Exclusão deste Doutor?')">               
            {{ csrf_field() }}
            {{ method_field('delete') }}
            <input type="submit" class="btn btn-danger btn-sm" 
                  value="Excluir">
          </form>                   
        </td>
  </tr>

    @endforeach

  </tbody>
</table>
@endsection