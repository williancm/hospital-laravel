@extends('admin.modelo') 

@section('conteudo')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

@if ($acao == 1)
  <h3>Inclusão de doutores</h3>
  <form method="post" action="{{ route('doutores.store') }}" enctype="multipart/form-data">

@elseif ($acao == 2)
  <h3>Alteração de doutores</h3>
  <form method="post" action="{{ route('doutores.update', $reg->id) }}" enctype="multipart/form-data">
  {{ method_field('put') }} 
    

@endif 

{{ csrf_field() }}

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Nome do Doutor:</label>
            <input type="text" class="form-control" id="nome" name="nome" 
                  value="{{$reg->nome or old('nome')}}" autofocus> 
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="setor_id">Setor</label>
          <select id="setor_id" name="setor_id" class="form-control">
            @foreach($setors as $m)
              <option value="{{$m->id}}" 
                      {{ ((isset($reg) and $reg->setor_id == $m->id) or 
                         old('setor_id') == $m->id) ? "selected" : "" }}>
                      {{$m->setor}}</option>
            @endforeach
          </select>  
        </div>
      </div>
    </div>

          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="cpf">Cpf:</label>
                <input type="text" class="form-control" id="cpf" name="cpf"
                      value="{{$reg->cpf or old('cpf')}}"> 
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="sala">Sala:</label>
                <input type="text" class="form-control" id="sala" name="sala"
                      value="{{$reg->sala or old('sala')}}"> 
              </div>
            </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="precoConsulta">Preço da Consulta:</label>
                <input type="text" class="form-control" id="precoConsulta" name="precoConsulta"
                      value="{{$reg->precoConsulta or old('precoConsulta')}}"> 
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="email">E-mail:</label>
                <input type="text" class="form-control" id="email" name="email"
                      value="{{$reg->email or old('email')}}" >
              </div>
            </div>
          </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label for="foto">Foto:</label>
                <input type="file" class="form-control" id="foto" name="foto">
              </div>
            </div>
          </div>

          <input type="submit" value="Enviar" class="btn btn-danger">
        </form>
      </div>
    </div>

@endsection