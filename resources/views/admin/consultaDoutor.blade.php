@extends('admin.modelo') 
@section('conteudo')

<div class="row">
  <div class="col-sm-10">
     <h3>Consultas Marcadas</h3>
  </div>   
</div>

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
 google.charts.load("current", {packages:["corechart"]});
 google.charts.setOnLoadCallback(drawChart);

 function drawChart() {
    var data = google.visualization.arrayToDataTable([['Doutor', 'Saldo']
    @foreach ($linhas as $linha)
      {!! ",['$linha->nome', $linha->saldo]" !!}
    @endforeach
    ]);
    var options = {
      title: 'Percentual do Saldo de cada Doutor em relação ao lucro gerado pelo hospital',
      is3D: true,
    };
    var chart = new google.visualization
    .PieChart(document.getElementById('piechart_3d'));
    chart.draw(data, options);
 }
</script>
<div id="piechart_3d" style="width: 900px; height: 500px;"></div>

<table class="table table-hover">
  <thead>
    <tr>
    <th>Saldo</th>
      <th>Nome do Doutor</th>
      <th>Setor</th>
      <th>Total de Pacientes</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($linhas as $linha)
    <tr>
      <td> R$ {{ $linha->saldo }} </td> 
      <td> {{ $linha->nome }} </td> 
      <td> {{ $linha->setor->setor }} </td>
      <td> {{ $linha->qtd }} </td> 
  </tr>

    @endforeach

  </tbody>
</table>


@endsection