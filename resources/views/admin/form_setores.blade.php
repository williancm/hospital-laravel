@extends('admin.modelo') 

@section('conteudo')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

  <h3>Inclusão de Setor</h3>
  <form method="post" action="{{ route('doutores.saveSetor') }}">

    {{ csrf_field() }}
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="setor_id">Setores já cadastrados</label>
          <select id="setor_id" name="setor_id" class="form-control" readonly="readonly">
            @foreach($setors as $m)
              <option value="{{$m->id}}" 
                      {{ ((isset($reg) and $reg->setor_id == $m->id) or 
                         old('setor_id') == $m->id) ? "selected" : "" }}>
                      {{$m->setor}}</option>
            @endforeach
          </select>  
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="setor">Nome do Setor:</label>
            <input type="text" class="form-control" id="setor" name="setor" 
                  value="{{$reg->setor or old('setor')}}" autofocus> 
        </div>
      </div>
    </div>
    
    <input type="submit" value="Enviar" class="btn btn-danger">
  </form>

@endsection