<div class="container-fluid">
    <h4 style="text-align: center">Revenda Herbie</h4>
    <h5 style="text-align: center">Relatório de Veículos Cadastrados</h5>
    <table class="table table-bordered table-sm">
        <thead class="thead-light">
            <tr>
                <th>Nome</th>
                <th>Setor</th>
                <th>E-mail</th>
                <th>Total de Pacientes</th>
                <th>Saldo Total</th>
                <th>Foto</th>
            </tr>
            </thead>
        <tbody>
            @foreach($linhas as $linha)
                <tr>
                    <td>{{$linha->nome}}</td>
                    <td>{{$linha->setor->setor}}</td>
                    <td>{{$linha->email}}</td>
                    <td>{{$linha->qtd}}</td>
                    <td>R$ {{$linha->saldo}}</td>
                    @if (Storage::exists($c->foto))
                        <img src="{{public_path('storage/'.$linha->foto)}}" style="width:100px;height:60px">
                    @else
                        <img src="{{public_path('storage/fotos/semfoto.png')}}">
                    @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
        </table>
</div>