<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CtDoutors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doutors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('cpf', 60);
            $table->string('sala', 60);
            $table->decimal('precoConsulta', 10, 2);
            $table->string('email', 100);
            $table->integer('setor_id')->unsigned();;
            $table->string('foto', 100);
            $table->timestamps();

            // define relacionamento com a tabela (model) de setors
            $table->foreign('setor_id')->references('id')->on('setors')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doutors');
    }
}
