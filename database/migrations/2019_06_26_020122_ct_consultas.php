<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CtConsultas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data', 100);
            $table->integer('doutor_id')->unsigned();;
            $table->string('nome', 100);
            $table->smallInteger('idade');
            $table->string('cpf', 14);
            $table->string('email', 100);

            // define relacionamento com a tabela (model) de doutors
            $table->foreign('doutor_id')->references('id')->on('doutors')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas');
    }
}
