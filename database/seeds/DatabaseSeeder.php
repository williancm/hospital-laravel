<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SetorsTableSeeder::class);
        $this->call(DoutorsTableSeeder::class);
    }
}

class SetorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setors')->insert(['setor'=>'Cardiologia']);
        DB::table('setors')->insert(['setor'=>'Acupuntura']);
        DB::table('setors')->insert(['setor'=>'Dermatologia']);
        DB::table('setors')->insert(['setor'=>'Medicina esportiva']);
        DB::table('setors')->insert(['setor'=>'Otorrinolaringologia']);
        DB::table('setors')->insert(['setor'=>'Pediatria']);
        DB::table('setors')->insert(['setor'=>'Oftalmologia']);
    }
}

class DoutorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doutors')->insert([
            'nome' => 'Willian Matheus',
            'cpf' => '041.809.700-31',
            'sala' => '104B',
            'precoConsulta' => 170,
            'email' => 'willianmatheuscm@gmail.com',
            'setor_id' => 4,
            'foto' => '',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('doutors')->insert([
            'nome' => 'Guilherme Tavares',
            'cpf' => '044.849.703-32',
            'sala' => '102B',
            'precoConsulta' => 320,
            'email' => 'tavaresguilherme@gmail.com',
            'setor_id' => 1,
            'foto' => '',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
    }
}
