var tbDoutors = document.getElementById("tbDoutors");

var inCodigo = document.getElementById("inCodigo");
var inNome = document.getElementById("inNome");
var inSetor = document.getElementById("inSetor");
var inEmail = document.getElementById("inEmail");
var inPreco = document.getElementById("inPreco");

function carregarCarros() {

  var url = "https://mysterious-fortress-85760.herokuapp.com/api/doutores";

  fetch(url)
    .then(resp => resp.json())
    .then(function (data) {
      mostraDoutors(data);
    });
}
window.addEventListener("load", carregarDoutors);

function mostraDoutors(doutors) {

  for (var c of doutors) {
    var linha = tbDoutors.insertRow(-1);

    linha.insertCell(0).textContent = c.id;
    linha.insertCell(1).textContent = c.nome;
    linha.insertCell(2).textContent = c.setor;
    linha.insertCell(3).textContent = c.email;
    linha.insertCell(4).textContent = c.preco;    
  }
}

function adicionar() {

  var url = "https://mysterious-fortress-85760.herokuapp.com/api/doutores";
  var data = "nome="+inNome.value+"&setor="+inSetor.value+"&email="+inEmail.value+"&preco="+inPreco.value;

  fetch(url, {
    method: 'post',
    headers: {
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
    },
    body: data,
  })
    .then(resp => resp.json())
    .then(function() {
      alert('Ok! Registro Inserido! Atualize a página');      
    })
    .catch(function (error) {
      alert('Erro na inclusão...' + error);
    });
}
btAdicionar.addEventListener("click", adicionar);