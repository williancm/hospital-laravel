<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doutor extends Model
{
    public $timestamps = false;
    protected $fillable = ['nome', 'cpf', 'sala', 'precoConsulta', 
                            'email', 'setor_id', 'foto'];

    public function setor() {
        return $this->belongsTo('App\Setor');
    }
}
