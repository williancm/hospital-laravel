<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvisoConsulta extends Mailable
{
    use Queueable, SerializesModels;

    private $consulta;

    public function __construct($consulta)
    {
        $this->consulta = $consulta;
    }

    public function build()
    {
        return $this->view('mail.aviso', ['consulta' => $this->consulta]);
    }
}