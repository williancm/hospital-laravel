<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    public $timestamps = false;
    protected $fillable = ['setor'];
}
