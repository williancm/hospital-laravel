<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Doutor;
use App\Consulta;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function inicio()
    {
        $linhas = Doutor::orderBy('nome')->get();
        return view('cliente.inicio', ['reg' => $linhas]);
    }

    public function show($id)
    {
        // procura (e posiciona) no registro cujo id foi passado como parâmetro
        $reg = Doutor::find($id);

        return view('cliente.view_doutor', ['reg' => $reg]);
    }

    public function createConsulta($id)
    {
        // procura (e posiciona) no registro cujo id foi passado como parâmetro
        $reg = Doutor::find($id);

        return view('cliente.form_consulta', ['reg' => $reg]);
    }

    public function saveConsulta(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nome'             => 'required|max:255',
            'cpf'              => 'required',
            'idade'            => 'required',
            'data'            => 'required|unique:consultas',
            'email'            => 'required'
        ]);

        $dados = $request->all();
        $dados['doutor_id'] = $id;
        $consulta = Consulta::create($dados);
        if ($consulta) {
            return redirect()->route('home.inicio')
                   ->with('status', 'Ok! Consulta Marcada');
        } else {
            return redirect()->route('home.inicio')
                   ->with('status', 'Erro... Consulta não Marcada...');
        }      
    }

    public function pesquisa(Request $request)
    {
        $palavra = $request->palavra;
        // obtém os registros cadastrados na tabela candidatas
        $linhas = Doutor::orderBy('nome')
                        ->where('nome', 'like', '%'.$palavra.'%')->get();
        $count=count($linhas);
        if($count > 0){
            return view('cliente.inicio', ['reg' => $linhas, 'status' => 'Foram encontrados '. $count.' doutores com a Palavra-Chave']);
        }else{ 
            $linhas = Doutor::orderBy('nome')->get();
            return view('cliente.inicio', ['reg' => $linhas, 'status' => 'Não há doutores com a Palavra-Chave']);
        }
    }
}
