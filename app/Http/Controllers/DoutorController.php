<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Doutor;
use App\Setor;
use App\Consulta;

class DoutorController extends Controller
{
    public function index()
    {
        // obtém os registros cadastrados na tabela candidatas
        $linhas = Doutor::orderBy('nome')->get();

        return view('admin.lista_doutores', ['linhas' => $linhas]);
    }

    public function create()
    {
        $setors = Setor::orderBy('setor')->get();
        return view('admin.form_doutores', ['acao'=>1, 'setors'=>$setors]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nome'             => 'required|max:255',
            'cpf'              => 'required|unique:doutors',
            'sala'             => 'required',
            'precoConsulta'    => 'integer|required',
            'email'            => 'required|unique:doutors',
            'setor_id'         => 'required'
        ]);
        // obtém todos os campos do formulário
        $dados = $request->all();

        // armazena a foto em public/storage/fotos
        $path = $request->file('foto')->store('fotos', 'public');

        // obtém o caminho da foto
        $dados['foto'] = $path; 

        // insere o registro (com as definições do fillable (na Model) e com os 
        // nomes de campos do formulário idênticos ao da tabela)
        $reg = Doutor::create($dados);

        if ($reg) {
            return redirect()->route('doutores.index')
                   ->with('status', 'Ok! Doutor Inserido com Sucesso');
        } else {
            return redirect()->route('doutores.index')
                   ->with('status', 'Erro... Doutor Não Inserido...');
        }
    }

    public function show($id)
    {
        // procura (e posiciona) no registro cujo id foi passado como parâmetro
        $reg = Doutor::find($id);

        return view('admin.view_doutor', ['reg' => $reg]);
    }

    public function edit($id)
    {
        // procura (e posiciona) no registro cujo id foi passado como parâmetro
        $reg = Doutor::find($id);
        $setors = Setor::orderBy('setor')->get();
        return view('admin.form_doutores', ['reg' => $reg, 'setors'=>$setors, 'acao' => 2]);
    }

    public function update(Request $request, $id)
    {
        // obtém os campos do form
        $dados = $request->all();

        // posiciona no registro a ser alterado
        $reg = Doutor::find($id);

        // se alterou a foto... realiza a alteração da foto
        if (isset($dados['foto'])) {
          // armazena a foto em public/storage/fotos
          $path = $request->file('foto')->store('fotos', 'public');

          // obtém o caminho da foto
          $dados['foto'] = $path; 

          // obtém o caminho da foto antiga
          $antiga = $reg->foto;
          // exclui a foto antiga
          Storage::disk('public')->delete($antiga);  
        }

        // altera o registro com os novos dados do form
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('doutores.index')
                   ->with('status', 'Ok! Doutor Alterado com Sucesso');
        } else {
            return redirect()->route('candidatas.index')
                   ->with('status', 'Erro... Doutor Não Alterado...');
        }        
    }

    public function destroy($id)
    {
        // posiciona no registro a ser excluído
        $reg = Doutor::find($id);

        // obtém o caminho da foto
        $foto = $reg->foto;
        // exclui a foto 
        Storage::disk('public')->delete($foto);  

        if ($reg->delete()) {
            return redirect()->route('doutores.index')
                   ->with('status', 'Ok! Doutor Excluído com Sucesso');
        } else {
            return redirect()->route('doutores.index')
                   ->with('status', 'Erro... Doutor Não Excluído...');
        }
    }


    public function createSetor()
    {
        $setors = Setor::orderBy('setor')->get();
        return view('admin.form_setores', ['setors'=>$setors]);
    }
    
    public function saveSetor(Request $request)
    {
        $validatedData = $request->validate([
            'setor'     => 'required|unique:setors'
        ]);
        $dados = $request->all();

        $reg = Setor::create($dados);

        if ($reg) {
            return redirect()->route('doutores.index')
                   ->with('status', 'Ok! Setor Inserido com Sucesso');
        } else {
            return redirect()->route('doutores.index')
                   ->with('status', 'Erro... Setor Não Inserido...');
        }
    }

    public function viewConsultas()
    {
        $consultas = Consulta::orderBy('data')->get();
        return view('admin.view_consultas', ['linhas'=> $consultas]);
    }

    public function consultaDoutor()
    {
        $linhas = Doutor::orderBy('id')->get();
        foreach($linhas as $linha){
            $linha['qtd'] = Consulta::where('doutor_id', $linha->id)->count();
            $linha['saldo'] = $linha->qtd * $linha->precoConsulta;
        }

        $linhas = $linhas->sortByDesc('saldo');

        return view('admin.consultaDoutor', ['linhas'=> $linhas]);
    }
    public function relatorio (){
        $linhas = Doutor::orderBy('id')->get();
        foreach($linhas as $linha){
            $linha['qtd'] = Consulta::where('doutor_id', $linha->id)->count();
            $linha['saldo'] = $linha->qtd * $linha->precoConsulta;
        }

        $linhas = $linhas->sortByDesc('saldo');

        return \PDF::loadView('relatorio', ['linhas'=>$linhas])
                                                    ->stream();
    }

    public function email($id){
        $linhas = Consulta::find($id);
        $destino = $linhas['email'];
        
        Mail::to($destino)->send(new AvisoConsulta($linhas));

        return redirect()->route('candidatas.index')
                   ->with('status', 'Ok! Email enviado com Sucesso');

    }

}
