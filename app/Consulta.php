<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    public $timestamps = false;
    protected $fillable = ['data', 'doutor_id', 'nome', 'idade', 
                            'cpf', 'email'];

    public function doutor() {
        return $this->belongsTo('App\Doutor');
    }
    
}
